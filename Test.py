
def saisieNombre (invite):
    x = None
    while not x:
        try:
            x = int(input(invite))
        except ValueError:
            print('Entrée invalide.')
    return x

def creerLabyrinthe (n, m):
    labyrinthe = [[-1] * m for _ in range(n)]
    compteur = 0
    for i in range(1, n - 1, 2):
        for j in range(1, m - 1, 2):
            labyrinthe[i][j] = compteur
            compteur += 1


    return labyrinthe

def afficheLabyrinthe(labyrinthe):
    for ligne in labyrinthe:
        for col in ligne:
            if col==-1:
                print(' -1  ', sep="", end="")
            else:
                print('{:03d}  '.format(col), sep="", end="")
        print('')

n = saisieNombre('Nombre de lignes : ')
n = (n * 2 + 1)
m = saisieNombre('Nombre de colonnes : ')
m = m * 2 + 1

labyrinthe = creerLabyrinthe(n, m)

afficheLabyrinthe(labyrinthe)


print("")

print(labyrinthe)
